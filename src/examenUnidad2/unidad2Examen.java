package examenUnidad2;


import examenUnidad2.Recibo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author REDES
 */
public class unidad2Examen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Recibo recibo=new Recibo();
        recibo.setNumRecibo(23);
        recibo.setNombre("Jose Lopez Acosta");
        recibo.setPuesto(2);
        recibo.setNivel(1);
        recibo.setDiasTrabajados(10);
        
        System.out.println("Pago: "+recibo.calcularPago());
        System.out.println("Impuesto: "+recibo.calcularImpuesto());
        System.out.println("Total: "+recibo.calcularTotal());
    }
    
}
