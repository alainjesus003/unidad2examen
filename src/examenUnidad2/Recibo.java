package examenUnidad2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author REDES
 */
public class Recibo {
    private int numRecibo;
    private String nombre;
    private int puesto;
    private int nivel;
    private int diasTrabajados;
    
    public Recibo(){
        this.numRecibo=0;
        this.nombre="";
        this.puesto=0;
        this.nivel=0;
        this.diasTrabajados=0;
    }

    public Recibo(int numRecibo, String nombre, int puesto, int nivel, int diasTrabajados) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.puesto = puesto;
        this.nivel = nivel;
        this.diasTrabajados = diasTrabajados;
    }
    
    public Recibo(Recibo otro) {
        this.numRecibo = otro.numRecibo;
        this.nombre = otro.nombre;
        this.puesto = otro.puesto;
        this.nivel = otro.nivel;
        this.diasTrabajados = otro.diasTrabajados;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    
    //metodos de comportamiento
    public float calcularPago(){
        float pago=0.0f;
        switch(this.puesto){
            case 1:
                pago=100*this.diasTrabajados;
                break;
            case 2:
                pago=200*this.diasTrabajados;
                break;
            case 3:
                pago=300*this.diasTrabajados;
                break;
            default:
                System.out.println("opcion no valida");
                break;
        }
        return pago;
    }
    
    public float calcularImpuesto(){
        float impuesto=0.0f;
        switch(this.nivel){
            case 1:
                impuesto=this.calcularPago()*.05f;
                break;
            case 2:
                impuesto=this.calcularPago()*.03f;
                break;
            default:
                System.out.println("opcion no valida");
                break;
        }
        return impuesto;
    }
    
    public float calcularTotal(){
        float total=0.0f;
        total=this.calcularPago()-this.calcularImpuesto();
        return total;
    }
    
    
}
